const process = require("process");
module.exports = {
    stories: ['../src/**/*.stories.tsx'],
    addons: [
      '@storybook/preset-create-react-app',
      '@storybook/addon-actions',
      '@storybook/addon-links',
      '@storybook/addon-knobs',
    ],
    webpackFinal: (config) => {
      config.resolve.modules.push(process.cwd() + "/node_modules");
      config.resolve.modules.push(process.cwd() + "/src");

      // this is needed for working w/ linked folders
      config.resolve.symlinks = false;
      return config;
    }
  };
