import "../src/assets/css/global.styles.css";

export const parameters = {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: { disabled: true },
  }