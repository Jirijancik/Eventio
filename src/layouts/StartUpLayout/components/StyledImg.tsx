import { Color } from 'assets/css/colors';
import styled from 'styled-components';

export const StyledImg = styled.img`
  width: auto;
  height: 100vh;
  background: ${Color.Black};
  background-blend-mode: multiply
`;
