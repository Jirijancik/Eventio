import styled from 'styled-components';

export const StyledStartUpWrapper = styled.div`
  min-width: 240px;
  width: 100vw;
  height: 100vh;
  display: grid;
  overflow: hidden;
  grid-template-columns:  fit-content(0) auto fit-content(0);
  justify-content: space-between;
  @media only screen and (max-width: 1050px) {
  & {
    grid-template-columns: 1fr;
    grid-auto-flow: row;
    justify-items: center;
    align-items: center;
  }
}
`;
