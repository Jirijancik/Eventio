import React from 'react';
import PropTypes from 'prop-types';
import image from 'assets/images/Image.png';
import { Link } from 'components/Link/Link/Link';
import { Alignment } from 'layouts/wrappers/enums/alignment';
import { Color } from 'assets/css/colors';
import { Logo, StormTrooper } from 'assets/svgs';
import { useViewport } from 'utils/useVieport';
import { Stack } from 'layouts/wrappers/Stack';
import { StyledImg } from './components/StyledImg';
import { StyledStartUpWrapper } from './components/StyledStartUpWrapper';
import { StyledRow } from './components/StyledRow';

export interface IStartUpLayoutProps {
  readonly showStormtrooper?: boolean
}

const propTypes = {
  children: PropTypes.node,
  showStormtrooper: PropTypes.bool,
};

export const StartUpLayout: React.FC<IStartUpLayoutProps> = (props) => {
  const {
    children,
    showStormtrooper = false,
  } = props;

  const { width } = useViewport();
  const breakpoint = 1100;

  return (
    <>
      <Logo
        fill={Color.White}
        opacity={1}
        width={29}
        height={28}
        style={{ position: 'absolute', top: 40, left: 40 }}
      />
      <StyledStartUpWrapper>
        { width > breakpoint
        && (
          <>
            <StyledImg src={image} alt="startupimage" />
            <div style={{
              position: 'absolute', bottom: '5vh', left: '5vh', color: Color.White,
            }}
            >
              <Stack spacing={15} alignY={Alignment.Center}>
                <h3 style={{ fontSize: '3vh', fontWeight: 400 }}>“Great, kid. Don’t get cocky.”</h3>
                <hr style={{ borderColor: Color.Green, width: 12, height: 2 }} />
                <p style={{ fontSize: '2vh', color: Color.GrayDark }}>Han Solo</p>
              </Stack>
            </div>
          </>
        )}

        {children}

        <StyledRow spacing={3} alignY={Alignment.Start} {...props}>
          <p style={{ color: Color.Gray }}>Don’t have account?</p>
          <Link linkPath="/sign-up" text="SIGN IN" />
        </StyledRow>
      </StyledStartUpWrapper>

      {showStormtrooper
      && (
      <StormTrooper
        fill={Color.DakrBlue}
        opacity={0.3}
        width={445}
        height={432}
        style={{ position: 'absolute', top: '35%', left: '20%' }}
      />
      )}
    </>
  );
};

StartUpLayout.displayName = 'StartUpLayout';
StartUpLayout.propTypes = propTypes;
