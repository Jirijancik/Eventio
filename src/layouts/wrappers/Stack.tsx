import React, { forwardRef } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Alignment } from './enums/alignment';

export interface IStackProps {
  readonly alignX?: Alignment;
  readonly alignY?: Alignment;
  readonly spacing?: number;
  readonly children?: React.ReactNode;
}

const propTypes = {
  alignX: PropTypes.oneOf(Object.values(Alignment)),
  alignY: PropTypes.oneOf(Object.values(Alignment)),
  spacing: PropTypes.number,
  children: PropTypes.node,
};

const StyledStack = styled.div<IStackProps>`
  align-items: ${({ alignY }) => alignY};
  justify-content: ${({ alignX }) => alignX};
  display: flex;
  flex-direction: column;
  gap: ${({ spacing }) => `${spacing}px`};
`;

export const Stack = forwardRef<HTMLDivElement, IStackProps>((props, ref) => {
  const {
    alignX = Alignment.Start,
    alignY = Alignment.Start,
    spacing = 0,
  } = props;

  return (
    <StyledStack
      ref={ref}
      alignX={alignX}
      alignY={alignY}
      spacing={spacing}
      {...props}
    >
      {props.children}
    </StyledStack>
  );
});

Stack.displayName = 'Stack';
Stack.propTypes = propTypes;
