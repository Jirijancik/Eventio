import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Alignment } from './enums/alignment';

interface IRowProps {
  readonly alignX?: Alignment;
  readonly alignY?: Alignment;
  readonly spacing?: number;
  readonly wrap? : boolean;
  readonly width? : string;
}

const propTypes = {
  alignX: PropTypes.oneOf(Object.values(Alignment)),
  alignY: PropTypes.oneOf(Object.values(Alignment)),
  spacing: PropTypes.number,
  children: PropTypes.node,
  width: PropTypes.string,
  wrap: PropTypes.bool,
};

export interface IStyledRowProps extends Omit<IRowProps, 'children'> {
}

const StyledRow = styled.div<IStyledRowProps>`
  display: flex;
  flex-direction: row;
  flex-wrap: ${({ wrap }) => (wrap ? 'wrap' : 'no-wrap')};
  white-space: nowrap;
  width: ${({ width }) => (width ?? 'fit-content')};
  gap: ${({ spacing }) => `${spacing}px`};
  align-items: ${({ alignY }) => alignY};
  justify-content: ${({ alignX }) => alignX};
`;

export const Row: React.FC<IRowProps> = (props) => {
  const {
    alignX = Alignment.Start,
    alignY = Alignment.Center,
    children,
    spacing = 0,
    wrap = false,
  } = props;
  return (
    <StyledRow
      alignX={alignX}
      alignY={alignY}
      spacing={spacing}
      wrap={wrap}
      {...props}
    >
      {children}
    </StyledRow>
  );
};

Row.displayName = 'Row';
Row.propTypes = propTypes;
