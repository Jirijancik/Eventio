import { Color } from 'assets/css/colors';
import styled from 'styled-components';

export const StyledBasicLayoutWrapper = styled.div`
  background-color: ${Color.MainBackground};
  height: 100%;
  display: flex;
  align-items: center;
  place-content: center;
  flex-wrap: nowrap;
  overflow: hidden;
  box-sizing: border-box;
`;
