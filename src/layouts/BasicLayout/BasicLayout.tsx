import React from 'react';
import PropTypes from 'prop-types';
import { Logo } from 'assets/svgs';
import { Color } from 'assets/css/colors';
import { StyledBasicLayoutWrapper } from './components/StyledBasicLayoutWrapper';

export interface IBasicLayoutProps {
  readonly topLeftCornerContent: React.ReactNode
}

const propTypes = {
  children: PropTypes.node,
  topLeftCornerContent: PropTypes.node,
};

export const BasicLayout: React.FC<IBasicLayoutProps> = (props) => {
  const {
    children,
    topLeftCornerContent,
  } = props;

  return (
    <StyledBasicLayoutWrapper>
      <Logo
        fill={Color.Black}
        opacity={1}
        width={29}
        height={28}
        style={{ position: 'absolute', top: 40, left: 40 }}
      />
      <div style={{ position: 'absolute', top: 40, right: 40 }}>
        {topLeftCornerContent}
      </div>
      {children}
    </StyledBasicLayoutWrapper>
  );
};

BasicLayout.displayName = 'BasicLayout';
BasicLayout.propTypes = propTypes;
