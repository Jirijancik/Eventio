import React from 'react';
import PropTypes from 'prop-types';
import { Color } from 'assets/css/colors';
import { Logo } from 'assets/svgs';
import { Row } from 'layouts/wrappers/Row';
import { UserMenu } from 'components/UserMenu.tsx';
import { IconButton } from 'components/Button';
import { ButtonStyle } from 'components/Button/enums/buttonStyleEnum';
import { ButtonSize } from 'components/Button/enums/buttonSizesEnum';
import { AiOutlinePlus } from 'react-icons/ai';
import { useAuth } from 'contexts/AuthContext';
import { useHistory, useLocation } from 'react-router-dom';
import { StyledContent } from './components/StyledContent';
import { StyledDashboardWrapper } from './components/StyledDashboardWrapper';

export interface IDashboardLayoutProps {
  readonly topNavContent?: React.ReactNode;
}

const propTypes = {
  children: PropTypes.node,
  topNavContent: PropTypes.node,
};

export const DashboardLayout: React.FC<IDashboardLayoutProps> = (props) => {
  const {
    children,
    topNavContent,
  } = props;

  const { currentUser } = useAuth();
  const history = useHistory();
  const location = useLocation();

  const handleOnCreateNewEvent = () => {
    history.push(`${location.pathname}/new-event`);
  };

  return (
    <StyledDashboardWrapper>
      <>
        <Row>
          <Logo fill={Color.Black} opacity={1} width={29} height={28} style={{ position: 'absolute', top: 40, left: 40 }} />
          <div style={{ position: 'absolute', top: 40, right: 40 }}>
            {!!currentUser
             && (
             <UserMenu
               firstName={currentUser.firstName}
               lastName={currentUser.lastName}
             />
             )}
          </div>
        </Row>

        <StyledContent>
          {!!topNavContent && topNavContent}
          {children}
        </StyledContent>

        <div style={{ position: 'absolute', bottom: 40, right: 40 }}>
          <IconButton
            label="Add New Event"
            onClick={handleOnCreateNewEvent}
            style={ButtonStyle.Secondary}
            size={ButtonSize.Large}
            renderIcon={() => <AiOutlinePlus />}
          />
        </div>
      </>
    </StyledDashboardWrapper>
  );
};

DashboardLayout.displayName = 'DashboardLayout';
DashboardLayout.propTypes = propTypes;
