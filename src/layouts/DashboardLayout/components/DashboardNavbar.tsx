import React from 'react';
import { Row } from 'layouts/wrappers/Row';
import { IconButton } from 'components/Button';
import { ButtonSize } from 'components/Button/enums/buttonSizesEnum';
import { BsFillGrid3X2GapFill, BsViewStacked } from 'react-icons/bs';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { NavLink } from 'components/Link/';

export interface IDashboardNavbarProps {
  readonly onToggle: (event: boolean) => void;
  readonly isMobile: boolean
}

const propTypes = {
  onToggle: PropTypes.func.isRequired,
  isMobile: PropTypes.bool.isRequired,
};

export const StyledNavwabrWrapper = styled(Row)`
width: 100%;
justify-items: center;
display: flex;
justify-content: space-between;
`;

export const DashboardNavbar: React.FC<IDashboardNavbarProps> = ({ onToggle, isMobile }) => (
  <StyledNavwabrWrapper>
    <Row spacing={32}>
      <NavLink linkPath="/dashboard" text="ALL EVENTS" />
      <NavLink linkPath="/dashboard/future-events" text="FUTURE EVENTS" />
      <NavLink linkPath="/dashboard/past-events" text="PAST EVENTS" disabled />
    </Row>

    {!isMobile && (
    <Row spacing={8}>
      <IconButton
        label="Display Grid"
        onClick={() => onToggle(true)}
        size={ButtonSize.Raw}
        renderIcon={() => <BsFillGrid3X2GapFill />}
      />
      <IconButton
        label="Display Rows"
        onClick={() => onToggle(false)}
        size={ButtonSize.Raw}
        renderIcon={() => <BsViewStacked />}
      />
    </Row>
    )}
  </StyledNavwabrWrapper>
);

DashboardNavbar.displayName = 'DashboardNavbar';
DashboardNavbar.propTypes = propTypes;
