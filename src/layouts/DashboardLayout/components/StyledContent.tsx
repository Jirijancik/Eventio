import styled from 'styled-components';

export const StyledContent = styled.div`
    display: flex;
    flex-direction: column;
    gap: 12px;
    min-width: 75vw;
`;
