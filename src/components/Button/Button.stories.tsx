/* eslint-disable no-console */
import React from 'react';
import { text, select, boolean } from '@storybook/addon-knobs';
import { Button } from './Button';
import { ButtonStyle } from './enums/buttonStyleEnum';
import { ButtonSize } from './enums/buttonSizesEnum';
import { IconButton, TextButton } from '.';

export const button = () => {
  const returnSignleSelectCaption = text('Single Select Caption', 'SIGN IN');
  const returnIsDisabled = boolean('Button is diasbled', false);
  const returnIsLoading = boolean('Button is loading', false);
  const returnButtonStyle = select('Button style', Object.values(ButtonStyle), ButtonStyle.Primary);
  const returnButtonSize = select('Button size', Object.values(ButtonSize), ButtonSize.Large);

  return (
    <Button
      text={returnSignleSelectCaption}
      onClick={() => null}
      isDisabled={returnIsDisabled}
      isLoading={returnIsLoading}
      style={returnButtonStyle}
      size={returnButtonSize}
    />
  );
};

export { iconButton } from './IconButton/IconButton.stories';
export { textButton } from './TextButton/TextButton.stories';

export default {
  title: `Button/${Button.displayName}`,
  component: Button,
  subcomponents: {
    IconButton,
    TextButton,
  },
};
