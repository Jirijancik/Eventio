import { Color } from 'assets/css/colors';
import { transition } from 'assets/css/transitions';
import styled from 'styled-components';

import { ButtonSize } from '../../enums/buttonSizesEnum';
import { getIconButtonSizes } from '../utils/getIconButtonSizes';

interface IStyledIconButtonProps {
  readonly isDisabled?: boolean;
  readonly isLoading?: boolean;
  readonly size: ButtonSize;
  readonly isRaw?: boolean;
}

export const StyledIconButton = styled.button<IStyledIconButtonProps>`
  background-color:  ${(props) => (props.isRaw ? 'transparent' : props.theme.backgroundColor)};
  color: ${(props) => props.theme.color};
  font-weight: 600;
  line-height: 32px; 
  letter-spacing: 1px;
  border-radius: 50%;
  display: grid;
  place-items: center;
  transition: ${() => transition};
  border: none;
  cursor: pointer;
  box-shadow: 0px 6px 9px 0px ${(props) => (props.isRaw ? 'transparent' : `${Color.Black}26`)};
  ${({ size }) => getIconButtonSizes(size)};
  &:hover{
    background-color: ${(props) => ((props.isDisabled || props.isLoading || props.isRaw) ? 'auto' : props.theme.hoverBackgroundColor)};
  }
`;
