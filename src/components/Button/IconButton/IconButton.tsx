import React, { MouseEvent } from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import { IButtonProps } from 'components/Button/Button';
import { StyledIconButton } from './components/StyledIconButton';
import { ButtonStyle } from '../enums/buttonStyleEnum';
import { getButtonColors } from '../utils/getButtonCollors';
import { ButtonType } from '../enums/buttonTypeEnum';
import { ButtonSize } from '../enums/buttonSizesEnum';

export interface IIconButtonProps extends IButtonProps {
  readonly label: string;
  readonly isDisabled?: boolean;
  readonly isLoading?: boolean;
  readonly onClick: (event: MouseEvent<HTMLButtonElement>) => void;
  readonly style?: ButtonStyle;
  readonly type?: ButtonType;
  readonly size?: ButtonSize;
  renderIcon: () => JSX.Element;
}

const propTypes = {
  isDisabled: PropTypes.bool,
  isLoading: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  style: PropTypes.oneOf(Object.values(ButtonStyle)),
  type: PropTypes.oneOf(Object.values(ButtonType)),
  size: PropTypes.oneOf(Object.values(ButtonSize)),
  renderIcon: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
};

export const IconButton: React.FC<IIconButtonProps> = (props) => {
  const {
    isDisabled,
    isLoading,
    style = ButtonStyle.Default,
    renderIcon,
    type = ButtonType.Button,
    size = ButtonSize.Large,
    onClick,
    label,
  } = props;

  return (
    <ThemeProvider theme={() => getButtonColors(style)}>
      <StyledIconButton
        isDisabled={isDisabled}
        isLoading={isLoading}
        type={type}
        size={size}
        onClick={onClick}
        isRaw={size === ButtonSize.Raw}
        aria-label={label}
      >
        {renderIcon()}
      </StyledIconButton>
    </ThemeProvider>
  );
};

IconButton.displayName = 'IconButton';
IconButton.propTypes = propTypes;
