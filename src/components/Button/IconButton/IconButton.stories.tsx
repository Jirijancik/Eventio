/* eslint-disable no-console */
import React from 'react';
import { text, select, boolean } from '@storybook/addon-knobs';
import { FaFilm } from 'react-icons/fa';
import { IconButton } from './IconButton';
import { ButtonStyle } from '../enums/buttonStyleEnum';
import { ButtonSize } from '../enums/buttonSizesEnum';

export const iconButton = () => {
  const returnSignleSelectCaption = text('Single Select Caption', 'SIGN IN');
  const returnIsDisabled = boolean('Button is diasbled', false);
  const returnIsLoading = boolean('Button is loading', false);
  const returnButtonStyle = select('Button style', Object.values(ButtonStyle), ButtonStyle.Primary);
  const returnButtonSize = select('Button size', Object.values(ButtonSize), ButtonSize.Large);
  return (
    <IconButton
      label="icon button"
      text={returnSignleSelectCaption}
      onClick={() => null}
      isDisabled={returnIsDisabled}
      isLoading={returnIsLoading}
      style={returnButtonStyle}
      size={returnButtonSize}
      renderIcon={() => <FaFilm />}
    />
  );
};
