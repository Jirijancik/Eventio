import { Color } from 'assets/css/colors';
import { ButtonStyle } from '../../enums/buttonStyleEnum';

export interface IIconButtonColors {
  readonly backgroundColor: string;
  readonly hoverBackgroundColor: string;
  readonly color?: string;
}

const primaryCollorSchema: IIconButtonColors = {
  backgroundColor: Color.Green,
  hoverBackgroundColor: Color.GreenDark,
  color: Color.White,
};

const defaultCollorSchema: IIconButtonColors = {
  backgroundColor: Color.Black,
  hoverBackgroundColor: Color.BlackLight,
  color: Color.White,
};

const destructiveCollorSchema: IIconButtonColors = {
  backgroundColor: Color.Pink,
  hoverBackgroundColor: Color.PinkDark,
  color: Color.White,
};

export const getIconButtonColors = (state: ButtonStyle) => {
  switch (state) {
    case ButtonStyle.Default:
      return defaultCollorSchema;
    case ButtonStyle.Primary:
      return primaryCollorSchema;
    case ButtonStyle.Destructive:
      return destructiveCollorSchema;
    default:
      throw new Error('There was problem with button state and its color.');
  }
};
