import { css } from 'styled-components';
import { ButtonSize } from '../../enums/buttonSizesEnum';

const getIconButtonSize = (buttonSize: number, horizontalPadding: number, minWidth: number, fontSize: number) => css`
    height: ${`${buttonSize}px`};
    width: ${`${buttonSize}px`};
    padding: 0 ${`${horizontalPadding}px`} ;
    min-width: ${`${minWidth}px`} ;
    font-size: ${`${fontSize}px`} ;
  `;

export const getIconButtonSizes = (size: ButtonSize) => {
  switch (size) {
    case ButtonSize.Large:
      return getIconButtonSize(56, 0, 0, 16);
    case ButtonSize.Small:
      return getIconButtonSize(32, 0, 0, 14);
    case ButtonSize.Raw:
      return getIconButtonSize(22, 0, 0, 16);
    default:
      throw new Error('Something went wronk while deciding Sizes in Button');
  }
};
