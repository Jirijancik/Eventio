import { css } from 'styled-components';
import { ButtonSize } from '../enums/buttonSizesEnum';

const getButtonSize = (buttonSize: number, horizontalPadding: number, minWidth: number, fontSize: number) => css`
  height: ${`${buttonSize}px`};
  padding: 0 ${`${horizontalPadding}px`} ;
  min-width: ${`${minWidth}px`} ;
  font-size: ${`${fontSize}px`} ;
`;

export const getButtonSizes = (size: ButtonSize) => {
  switch (size) {
    case ButtonSize.Large:
      return getButtonSize(57, 26, 240, 16);
    case ButtonSize.Small:
      return getButtonSize(32, 35, 100, 14);
    default:
      throw new Error('Something went wronk while deciding Sizes in Button');
  }
};
