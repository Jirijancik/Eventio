import { Color } from 'assets/css/colors';
import { ButtonStyle } from '../enums/buttonStyleEnum';

export interface IButtonColors {
  readonly backgroundColor: string;
  readonly hoverBackgroundColor: string;
  readonly color?: string;
}

const primaryCollorSchema: IButtonColors = {
  backgroundColor: Color.Green,
  hoverBackgroundColor: Color.GreenDark,
  color: Color.White,
};

const defaultCollorSchema: IButtonColors = {
  backgroundColor: Color.GrayMedium,
  hoverBackgroundColor: Color.Gray60,
  color: Color.GraySuperDark,
};

const destructiveCollorSchema: IButtonColors = {
  backgroundColor: Color.Pink,
  hoverBackgroundColor: Color.PinkDark,
  color: Color.White,
};

const secondaryCollorSchema: IButtonColors = {
  backgroundColor: Color.Black,
  hoverBackgroundColor: Color.BlackLight,
  color: Color.White,
};

export const getButtonColors = (state: ButtonStyle) => {
  switch (state) {
    case ButtonStyle.Default:
      return defaultCollorSchema;
    case ButtonStyle.Primary:
      return primaryCollorSchema;
    case ButtonStyle.Destructive:
      return destructiveCollorSchema;
    case ButtonStyle.Secondary:
      return secondaryCollorSchema;
    default:
      throw new Error('There was problem with button state and its color.');
  }
};
