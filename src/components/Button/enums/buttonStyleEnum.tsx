export enum ButtonStyle {
    Default = 'Default',
    Destructive = 'Destructive',
    Primary = 'Primary',
    Secondary = 'Secondary'
}
