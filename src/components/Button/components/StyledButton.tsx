import { transition } from 'assets/css/transitions';
import styled, { css } from 'styled-components';
import { ButtonSize } from '../enums/buttonSizesEnum';
import { getButtonSizes } from '../utils/getButtonSizes';

interface IStyledButtonProps {
  readonly isDisabled?: boolean;
  readonly isLoading?: boolean;
  readonly size: ButtonSize;
}

const inputLoadingStyles = css`
pointer-events: none;
filter: grayscale(0.55);
user-select: none;
display: grid;
place-items: center;
`;

export const StyledButton = styled.button<IStyledButtonProps>`
  background-color: ${(props) => props.theme.backgroundColor};
  color: ${(props) => props.theme.color};
  font-weight: 600;
  line-height: 32px; 
  letter-spacing: 1px;
  border-radius: 4px;
  transition: ${() => transition};
  border: none;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 500px;
  cursor: pointer;
  ${({ size }) => getButtonSizes(size)};
  ${({ isLoading }) => isLoading && inputLoadingStyles}
  &:hover{
    background-color: ${(props) => ((props.isDisabled || props.isLoading) ? 'auto' : props.theme.hoverBackgroundColor)};
  }
`;
