import { Color } from 'assets/css/colors';
import { ButtonStyle } from '../../enums/buttonStyleEnum';

export interface ITextButtonColors {
  readonly backgroundColor: string;
  readonly color?: string;
}

const primaryCollorSchema: ITextButtonColors = {
  backgroundColor: Color.Transparent,
  color: Color.Green,
};

const defaultCollorSchema: ITextButtonColors = {
  backgroundColor: Color.Transparent,
  color: Color.Black,
};

const destructiveCollorSchema: ITextButtonColors = {
  backgroundColor: Color.Transparent,
  //  hoverBackgroundColor: Color.PinkDark,
  color: Color.Pink,
};

export const getTextButtonColors = (state: ButtonStyle) => {
  switch (state) {
    case ButtonStyle.Default:
      return defaultCollorSchema;
    case ButtonStyle.Primary:
      return primaryCollorSchema;
    case ButtonStyle.Destructive:
      return destructiveCollorSchema;
    default:
      throw new Error('There was problem with button state and its color.');
  }
};
