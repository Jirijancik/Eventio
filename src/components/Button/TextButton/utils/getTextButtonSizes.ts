import { css } from 'styled-components';
import { ButtonSize } from '../../enums/buttonSizesEnum';

const getTextButtonSize = (fontSize: number) => css`
  font-size: ${`${fontSize}px`} ;
`;

export const getTextButtonSizes = (size: ButtonSize) => {
  switch (size) {
    case ButtonSize.Large:
      return getTextButtonSize(12);
    case ButtonSize.Small:
      return getTextButtonSize(10);
    default:
      throw new Error('Something went wronk while deciding Sizes in Button');
  }
};
