import { transition } from 'assets/css/transitions';
import styled from 'styled-components';
import { ButtonSize } from '../../enums/buttonSizesEnum';
import { getTextButtonSizes } from '../utils/getTextButtonSizes';

interface IStyledTextButtonProps {
  readonly isDisabled?: boolean;
  readonly isLoading?: boolean;
  readonly size: ButtonSize;
}

export const StyledTextButton = styled.button<IStyledTextButtonProps>`
  background-color: ${(props) => props.theme.backgroundColor};
  color: ${(props) => props.theme.color};
  font-weight: 600;
  line-height: 32px; 
  letter-spacing: 1px;
  display: grid;
  place-items: center;
  transition: ${() => transition};
  border: none;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  max-width: 500px;
  position: relative;
  cursor: pointer;
  ${({ size }) => getTextButtonSizes(size)};
`;
