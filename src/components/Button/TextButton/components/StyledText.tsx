import styled from 'styled-components';
import { transition } from 'assets/css/transitions';
import { StyledTextButton } from './StyledTextButton';

export const StyledText = styled.div`
  position: relative;
  transition: ${() => transition};
  ::after {
      content: "";
      position: absolute;
      width: 100%;
      height: 1px;
      bottom: 7px;
      right: 0px;
      background-color: transparent;
      transition: ${() => transition};
    }
  ${StyledTextButton}:hover &::after{
        content: "";
        position: absolute;
        width: 100%;
        height: 1px;
        bottom: 8px;
        right: 0px;
        background-color: ${(props) => props.theme.color};
        transition: ${() => transition};
    }
`;
