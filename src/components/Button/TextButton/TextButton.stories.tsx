/* eslint-disable no-console */
import React from 'react';
import { text, select, boolean } from '@storybook/addon-knobs';
import { FaFilm } from 'react-icons/fa';
import { TextButton } from './TextButton';
import { ButtonStyle } from '../enums/buttonStyleEnum';
import { ButtonSize } from '../enums/buttonSizesEnum';

export const textButton = () => {
  const returnSignleSelectCaption = text('Single Select Caption', 'SIGN IN');
  const returnIsDisabled = boolean('Button is diasbled', false);
  const returnButtonStyle = select('Button style', Object.values(ButtonStyle), ButtonStyle.Primary);
  const returnButtonSize = select('Button size', Object.values(ButtonSize), ButtonSize.Large);
  return (
    <TextButton
      onClick={() => null}
      isDisabled={returnIsDisabled}
      style={returnButtonStyle}
      size={returnButtonSize}
      renderIcon={() => <FaFilm />}
    >
      {returnSignleSelectCaption}
    </TextButton>
  );
};
