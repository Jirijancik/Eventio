import React, { MouseEvent } from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import { Row } from 'layouts/wrappers/Row';
import { StyledTextButton } from './components/StyledTextButton';
import { StyledText } from './components/StyledText';
import { ButtonStyle } from '../enums/buttonStyleEnum';
import { getTextButtonColors } from './utils/getTextButtonCollors';
import { ButtonType } from '../enums/buttonTypeEnum';
import { ButtonSize } from '../enums/buttonSizesEnum';

export interface ITextButtonProps {
  readonly isDisabled?: boolean;
  readonly onClick?: (event: MouseEvent<HTMLButtonElement>) => void;
  readonly style?: ButtonStyle;
  readonly type?: ButtonType;
  readonly size?: ButtonSize;
  renderIcon?: () => JSX.Element;
}

const propTypes = {
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
  style: PropTypes.oneOf(Object.values(ButtonStyle)),
  type: PropTypes.oneOf(Object.values(ButtonType)),
  size: PropTypes.oneOf(Object.values(ButtonSize)),
  renderIcon: PropTypes.func,
  children: PropTypes.string,
};

export const TextButton: React.FC<ITextButtonProps> = (props) => {
  const {
    isDisabled,
    style = ButtonStyle.Default,
    renderIcon,
    type = ButtonType.Button,
    size = ButtonSize.Large,
    children,
    onClick,
  } = props;

  return (
    <ThemeProvider theme={() => getTextButtonColors(style)}>
      <StyledTextButton
        isDisabled={isDisabled}
        type={type}
        size={size}
        onClick={onClick}
      >
        <Row spacing={8}>
          {!!renderIcon && renderIcon()}
          <StyledText>
            {children}
          </StyledText>
        </Row>
      </StyledTextButton>
    </ThemeProvider>
  );
};

TextButton.displayName = 'IconButton';
TextButton.propTypes = propTypes;
