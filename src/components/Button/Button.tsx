import React, { MouseEvent } from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider } from 'styled-components';
import { BiLoader } from 'react-icons/bi';
import { StyledButton } from './components/StyledButton';
import { ButtonStyle } from './enums/buttonStyleEnum';
import { getButtonColors } from './utils/getButtonCollors';
import { ButtonType } from './enums/buttonTypeEnum';
import { ButtonSize } from './enums/buttonSizesEnum';

export interface IButtonProps {
  readonly isDisabled?: boolean;
  readonly isLoading?: boolean;
  readonly onClick: (event: MouseEvent<HTMLButtonElement>) => void;
  readonly style?: ButtonStyle;
  readonly text?: string;
  readonly type?: ButtonType;
  readonly size?: ButtonSize;
  readonly form?: string;
}

const propTypes = {
  isDisabled: PropTypes.bool,
  isLoading: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  style: PropTypes.oneOf(Object.values(ButtonStyle)),
  text: PropTypes.string,
  form: PropTypes.string,
  type: PropTypes.oneOf(Object.values(ButtonType)),
  size: PropTypes.oneOf(Object.values(ButtonSize)),
};

export const Button: React.FC<IButtonProps> = (props) => {
  const {
    isDisabled,
    isLoading,
    style = ButtonStyle.Default,
    text,
    form,
    type = ButtonType.Button,
    size = ButtonSize.Large,
    onClick,
  } = props;

  return (
    <ThemeProvider theme={() => getButtonColors(style)}>
      <StyledButton
        isDisabled={isDisabled}
        isLoading={isLoading}
        type={type}
        size={size}
        onClick={onClick}
        form={form}
        aria-label={text}
      >
        {isLoading ? <BiLoader size={26} /> : text}
      </StyledButton>
    </ThemeProvider>
  );
};

Button.displayName = 'Button';
Button.propTypes = propTypes;
