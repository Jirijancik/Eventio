export enum InputTypesEnum {
    Email = 'email',
    Password = 'password',
    Text = 'text',
    Date = 'date',
    Time = 'time',
    Number = 'number'
}
