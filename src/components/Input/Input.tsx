import { IconButton } from 'components/Button';
import { ButtonSize } from 'components/Button/enums/buttonSizesEnum';
import { Stack } from 'layouts/wrappers/Stack';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { AiFillEye, AiFillEyeInvisible } from 'react-icons/ai';
import { StyledInput } from './components/StyledInput';
import { StyledLabel } from './components/StyledLabel';
import { InputTypesEnum } from './enums/InputTypesEnum';

export interface IInputProps {
  readonly type?: InputTypesEnum,
  readonly label?: string,
  readonly placeholder: string,
  readonly disabled?: boolean,
}

const propTypes = {
  type: PropTypes.oneOf(Object.values(InputTypesEnum)),
  label: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
};

export const Input = React.forwardRef<HTMLInputElement, IInputProps>((props, ref) => {
  const {
    type = InputTypesEnum.Text,
    label,
    placeholder,
    disabled = false,
  } = props;
  const [isPasswordVisible, setIsPasswordVisible] = useState(false);
  const [inputType, setInputType] = useState(type);

  const VisibilityIndicatorIcon = isPasswordVisible ? <AiFillEyeInvisible /> : <AiFillEye />;

  const handleShowPassword = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };

  useEffect(() => ((isPasswordVisible && type === InputTypesEnum.Password)
    ? setInputType(InputTypesEnum.Text)
    : setInputType(type)), [type, isPasswordVisible]);

  return (
    <Stack>
      <StyledLabel htmlFor="text-input">
        {label}
      </StyledLabel>
      <div style={{ position: 'relative' }}>
        <StyledInput
          placeholder={placeholder}
          onChange={() => setIsPasswordVisible(false)}
          id={label}
          type={inputType}
          ref={ref}
          disabled={disabled}
        />
        {type === InputTypesEnum.Password && (
        <div style={{ position: 'absolute', right: 5, top: 0 }}>
          <IconButton
            label="Show Password"
            onClick={handleShowPassword}
            renderIcon={() => VisibilityIndicatorIcon}
            size={ButtonSize.Raw}
          />
        </div>
        )}
      </div>
    </Stack>
  );
});

Input.propTypes = propTypes;
Input.displayName = 'Input';
