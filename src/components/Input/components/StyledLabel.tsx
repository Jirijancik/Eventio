import { Color } from 'assets/css/colors';
import { transition } from 'assets/css/transitions';
import styled from 'styled-components';

export const StyledLabel = styled.label`
width: 100%;
font-weight: normal;
font-size: 14px;
line-height: 24px;
transition: ${() => transition};
color: ${Color.AnotherDamnGray};
`;
