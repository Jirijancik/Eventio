import { Color } from 'assets/css/colors';
import { transition } from 'assets/css/transitions';
import styled from 'styled-components';

export const StyledInput = styled.input`
    min-width: 480px;
    max-width: 480px;
    border: none;
    border-bottom: 1px solid ${Color.Gray0}; 
    font-weight: normal;
    font-size: 18px;
    line-height: 24px;
    color: ${Color.Black};
    transition: ${() => transition};
    &:active, :focus{
        border-bottom: 1px solid ${Color.Black}; 
        outline: none;
    }

    &:invalid{
        border-bottom: 1px solid ${Color.Pink}; 
        outline: none;
    }

    &[disabled]{
        cursor: no-drop;
        background-color: ${Color.Gray0};
        border-bottom: 1px solid ${Color.Gray0}; 
    }

    &::placeholder{
        color: ${Color.Gray}
    }
`;
