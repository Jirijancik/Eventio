/* eslint-disable no-console */
import React from 'react';
import { text, select, boolean } from '@storybook/addon-knobs';
import { Input } from './Input';
import { InputTypesEnum } from './enums/InputTypesEnum';

export const link = () => {
  const returnPlaceholder = text('Input Placeholder', 'Placeholder');
  const returnLabel = text('Input Label', 'Random Label');
  const returnIsDisabled = boolean('Input is disabled', false);
  const returnInputType = select('Input type', InputTypesEnum, InputTypesEnum.Text);
  return (
    <form onSubmit={(e) => e.preventDefault()}>
      <Input
        label={returnLabel}
        placeholder={returnPlaceholder}
        type={returnInputType}
        disabled={returnIsDisabled}
      />
    </form>
  );
};

export default {
  title: `Input/${Input.displayName}`,
  component: Input,
};
