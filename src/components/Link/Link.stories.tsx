import React from 'react';
import {
  MemoryRouter,
} from 'react-router-dom';
import { Link } from './Link/Link';

export const link = () => (
  <MemoryRouter>
    <Link linkPath="/" text="Your Events" />
  </MemoryRouter>
);

export default {
  title: `Link/${Link.displayName}`,
  component: Link,
};
