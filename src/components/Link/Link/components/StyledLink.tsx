import styled, { css } from 'styled-components';
import {
  Link, LinkProps,
} from 'react-router-dom';
import { Color } from 'assets/css/colors';
import { transition } from 'assets/css/transitions';

interface IStyledNavLink extends LinkProps{
  readonly disabled: boolean
}

const linkStyle = css`
font-style: normal;
text-decoration: none;
font-weight: 500;
font-size: 14px;
line-height: 24px;
color: ${Color.AnotherDamnGray};
transition: ${() => transition};
  &[disabled] {
    pointer-events: none;
    cursor: no-drop;
  }
`;

export const StyledLink = styled(Link)`${linkStyle};` as React.FC<IStyledNavLink>;
