import React from 'react';
import PropTypes from 'prop-types';
import { StyledLink } from './components/StyledLink';

export interface ILinkProps {
  readonly linkPath: string;
  readonly text: string;
  readonly disabled?: boolean;
  readonly onClick?: () => void;
}

const propTypes = {
  linkPath: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

export const Link: React.FC<ILinkProps> = (props) => {
  const {
    linkPath,
    text,
    disabled = false,
    onClick,
  } = props;

  const handleOnClick = () => {
    if (onClick)onClick();
  };

  return (
    <StyledLink
      onClick={handleOnClick}
      disabled={disabled}
      aria-disabled={disabled}
      to={linkPath}
    >
      {text}
    </StyledLink>
  );
};

Link.displayName = 'Link';
Link.propTypes = propTypes;
