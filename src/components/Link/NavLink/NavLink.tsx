import React from 'react';
import PropTypes from 'prop-types';
import { Color } from 'assets/css/colors';
import { StyledNavLink } from './components/StyledNavlinkLink';
import { ILinkProps } from '../Link/Link';

const propTypes = {
  linkPath: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
};

export const NavLink: React.FC<ILinkProps> = (props) => {
  const {
    linkPath,
    text,
    disabled = false,
  } = props;

  return (
    <StyledNavLink
      disabled={disabled}
      activeStyle={{
        fontWeight: 'bold',
        color: Color.Black,
      }}
      exact
      to={linkPath}
    >
      {text}

    </StyledNavLink>
  );
};

NavLink.displayName = 'NavLink';
NavLink.propTypes = propTypes;
