import styled, { css } from 'styled-components';
import {
  NavLink, NavLinkProps,
} from 'react-router-dom';
import { Color } from 'assets/css/colors';
import { transition } from 'assets/css/transitions';

interface IStyledNavLink extends NavLinkProps{
  readonly disabled: boolean
}

const linkStyle = css<IStyledNavLink>`
  font-style: normal;
  text-decoration: none;
  font-weight: 500;
  font-size: 14px;
  line-height: 24px;
  color: ${Color.AnotherDamnGray};
  transition: ${() => transition};
  position:relative;
    &[disabled] {
      pointer-events: none;
      color: ${Color.Gray0}
    }
`;

export const StyledNavLink = styled(NavLink)`${linkStyle};` as React.FC<IStyledNavLink>;
