import React, { forwardRef } from 'react';
import styled from 'styled-components';
import { Stack } from 'layouts/wrappers/Stack';
import { Color } from 'assets/css/colors';

export interface IVerticalMenuProps {
  children: React.ReactNode;
}

export const StyledStack = styled(Stack)`
  background: #FFFFFF;
  box-shadow: 0px 5px 15px rgba(0, 0, 0, 0.198087);
  border-radius: 14px;
  width:fit-content;
  padding: 17px;
  position: relative;
  padding-left: 16px;
  font-family: var(--MAIN-FONT);
  &::before{
    content: '';
    position: absolute;
    top: -12px;
    right: 15px;
    width: 0; 
    height: 0; 
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-bottom: 12px solid ${Color.White};
  }
`;

export const VerticalMenu = forwardRef<HTMLDivElement, IVerticalMenuProps>((props, ref) => (
  <div ref={ref}>
    <StyledStack {...props} />
  </div>

));

VerticalMenu.displayName = 'VerticalMenu';
