import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'components/Link/Link/Link';
import { Row } from 'layouts/wrappers/Row';
import { Stack } from 'layouts/wrappers/Stack';
import { useAuth } from 'contexts/AuthContext';
import { StyledUserMenu } from './components/StyledUserMenu';
import { StyledStack } from './VerticalMenu/VerticalMenu';
import { StyledAvatar } from './components/StyledAvatar';

export interface IUserMenuProps {
   readonly firstName: string;
   readonly lastName: string;
}

const propTypes = {
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
};

export const UserMenu: React.FC<IUserMenuProps> = (props) => {
  const {
    lastName,
    firstName,
  } = props;

  const ref = useRef<HTMLDivElement>(document.createElement('div'));

  const [isOpened, setIsOpened] = useState(false);
  const { signOut } = useAuth();

  const userInitials = firstName.charAt(0) + lastName.charAt(0);

  const handleOnBlur: React.FocusEventHandler<HTMLButtonElement> = (event:any) => {
    if (ref && !(ref?.current?.contains(event?.relatedTarget as Node | null))) {
      setIsOpened(false);
    }
  };

  return (
    <Stack spacing={17}>
      <StyledUserMenu
        onClick={() => setIsOpened(!isOpened)}
        onBlur={handleOnBlur}
      >
        <Row spacing={8}>
          <StyledAvatar>
            {userInitials}
          </StyledAvatar>
          {' '}
          {firstName}
          {' '}
          {lastName}
          {' '}
          ▼
        </Row>
      </StyledUserMenu>
      {isOpened && (

      <StyledStack ref={ref}>
        <Link linkPath="/profile" text="Profile" />
        <Link onClick={signOut} linkPath="/login" text="Logout" />
      </StyledStack>

      )}
    </Stack>

  );
};

UserMenu.displayName = 'UserMenu';
UserMenu.propTypes = propTypes;
