import React from 'react';
import { text } from '@storybook/addon-knobs';
import {
  MemoryRouter,
} from 'react-router-dom';
import { UserMenu } from './UserMenu';

export const userMenu = () => {
  const returnUserMenuFirstName = text('First Name', 'Karel');
  const returnUserMenuLastName = text('Last Name', 'Černý');
  return (
    <MemoryRouter>
      <UserMenu firstName={returnUserMenuFirstName} lastName={returnUserMenuLastName} />
    </MemoryRouter>
  );
};

export default {
  title: `UserMenu/${UserMenu.displayName}`,
  component: UserMenu,

};
