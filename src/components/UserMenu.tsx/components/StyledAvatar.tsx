import { Color } from 'assets/css/colors';
import styled from 'styled-components';
import { transition } from 'assets/css/transitions';
import { StyledUserMenu } from './StyledUserMenu';

export const StyledAvatar = styled.div`
    border-radius: 50%;
    background-color: ${Color.GrayMedium};
    color: ${Color.GrayDark};
    font-size: 14px;
    height: 40px;
    width: 40px;
    transition: ${() => transition};
    display: grid;
    place-content: center;
    ${StyledUserMenu}:focus &{
        background-color: ${Color.GraySuperDark};
        color: ${Color.White};
        transition: ${() => transition};
    }
`;
