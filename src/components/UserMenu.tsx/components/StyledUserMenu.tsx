import { Color } from 'assets/css/colors';
import { transition } from 'assets/css/transitions';
import styled from 'styled-components';

export const StyledUserMenu = styled.button`
    background-color: transparent;
    color: ${Color.GrayDark};
    font-weight: 500;
    font-size: 14px;
    line-height: 24px;
    width: fit-content;
    border: none;
    padding: 0;
    transition: ${() => transition};
    cursor: pointer;
    &:focus {
        outline: none;
        color: ${Color.GraySuperDark};
        transition: ${() => transition};
        text-decoration: underline;
    }
`;
