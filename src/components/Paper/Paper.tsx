import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { StyledPaperWrapper } from './components/StyledPaperWrapper';

export interface IPaperProps {
  readonly spacing?: number | string,
  readonly children?: React.ReactNode
  readonly onClick?: any
}

const propTypes = {
  spacing: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  children: PropTypes.node,
  onClick: PropTypes.func,
};

export const Paper:React.FC<IPaperProps> = (props) => {
  const { spacing = 0, children } = props;
  const [spacingWithPx, setSpacingWithPx] = useState(`${spacing}px`);

  useEffect(() => (
    ((typeof spacing) === 'number')
      ? setSpacingWithPx(`${spacing}px`)
      : setSpacingWithPx(spacing as string)),
  [spacing]);

  return (
    <StyledPaperWrapper {...props} spacing={spacingWithPx}>
      {children}
    </StyledPaperWrapper>
  );
};

Paper.displayName = 'Paper';
Paper.propTypes = propTypes;
