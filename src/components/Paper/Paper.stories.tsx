/* eslint-disable no-console */
import React from 'react';
import { number } from '@storybook/addon-knobs';
import { Color } from 'assets/css/colors';
import { Paper } from './Paper';

export const link = () => {
  const returnSpacing = number('Spacing', 20);
  return (
    <Paper spacing={returnSpacing}>
      <div style={{
        width: 250,
        height: 150,
        backgroundColor: Color.BlackMedium,
        color: Color.White,
        display: 'grid',
        placeContent: 'center',
      }}
      >
        <span>RANDOM ITEM</span>
      </div>
    </Paper>

  );
};

export default {
  title: `Paper/${Paper.displayName}`,
  component: Paper,
};
