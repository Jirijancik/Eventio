import { Color } from 'assets/css/colors';
import styled from 'styled-components';

interface IStyledPaperWrapperProps {
  readonly spacing?: number | string;
}

export const StyledPaperWrapper = styled.div<IStyledPaperWrapperProps>`
    padding: ${({ spacing }) => spacing};
    background-color: ${Color.White};
    color: inherit;
    box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.108696);
    width: fit-content;
    box-sizing: border-box;
    border-radius: 2px;
`;
