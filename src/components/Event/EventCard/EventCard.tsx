import { Color } from 'assets/css/colors';
import { Stack } from 'layouts/wrappers/Stack';
import React from 'react';
import { Row } from 'layouts/wrappers/Row';
import { Paper } from 'components/Paper';
import PropTypes from 'prop-types';
import { StyledRow } from './components/StyledRow';
import { BaseEvent, IBaseEventProps } from '../BaseEvent';
import { StyledHeader } from './components/StyledHeader';

export interface IEventCardProps extends Omit<IBaseEventProps, 'renderEventComponent'>{
}

const propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  startsAt: PropTypes.string.isRequired,
  capacity: PropTypes.number.isRequired,
  attendees: PropTypes.number.isRequired,
  id: PropTypes.string.isRequired,
};

export const EventCard: React.FC<IEventCardProps> = (props) => {
  const {
    title,
    description,
    startsAt,
    capacity,
    attendees,
    id,
  } = props;

  const atendees = `${attendees} of ${capacity}`;

  return (
    <BaseEvent
      {...props}
      renderEventComponent={({
        ownerFullName, onClick, onKeyPress, EventButton,
      }) => (
        <div tabIndex={0} role="button" onClick={onClick} onKeyPress={onKeyPress} style={{ cursor: 'pointer' }}>
          <Paper spacing={32}>
            <Stack spacing={32}>
              <time
                dateTime={startsAt}
                style={{
                  color: Color.GrayDate,
                }}
              >
                {startsAt}
              </time>

              <Stack spacing={0}>
                <StyledHeader>{title}</StyledHeader>
                <p style={{ color: Color.BlackMedium }}>{ownerFullName}</p>
              </Stack>
              <p style={{ color: Color.GrayDark }}>{description}</p>
              <StyledRow>
                <Row>
                  <p style={{ color: Color.GrayDark }}>{atendees}</p>
                </Row>
                {EventButton(id)}
              </StyledRow>
            </Stack>
          </Paper>
        </div>

      )}
    />
  );
};

EventCard.displayName = 'EventCard';
EventCard.propTypes = propTypes;
