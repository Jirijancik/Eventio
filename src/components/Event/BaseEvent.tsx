import { Button } from 'components/Button';
import { ButtonSize } from 'components/Button/enums/buttonSizesEnum';
import { ButtonStyle } from 'components/Button/enums/buttonStyleEnum';
import { useAuth } from 'contexts/AuthContext';
import React, { useState, useEffect, useReducer } from 'react';
import { useHistory } from 'react-router-dom';
import { IUser } from 'services/authService';
import { eventService } from 'services/eventService';

export interface IBaseEventProps {
  readonly renderEventComponent: (props: any) => JSX.Element;
  title: string,
  description: string,
  startsAt: string,
  capacity: number,
  owner: IUser,
  attendees: number,
  id: string
}

const reducer = (state:any, action:any) => {
  switch (action.type) {
    case 'join':
      return {
        ...state, text: 'JOIN', style: ButtonStyle.Primary, onClick: (id:IUser['id']) => eventService.postAttendEvent(id),
      };
    case 'leave':
      return {
        ...state, text: 'LEAVE', style: ButtonStyle.Destructive, onClick: (id:IUser['id']) => eventService.deleteAttendEvent(id),
      };
    case 'edit':
      return {
        ...state, text: 'EDIT', style: ButtonStyle.Default,
      };
    default:
      return state;
  }
};

export const BaseEvent: React.FC<IBaseEventProps> = (props) => {
  const history = useHistory();

  const buttonState = {
    text: 'JOIN',
    style: ButtonStyle.Primary,
    onClick: (e: React.SyntheticEvent) => {
      e.stopPropagation();
      history.push(`/dashboard/event-edit/${props.id}`);
    },
  };

  const { renderEventComponent, owner } = props;
  const [ownerFullName, setOwnerFullName] = useState('');

  const [state, dispatch] = useReducer(reducer, buttonState);

  const handleOnClick = () => {
    history.push(`/dashboard/event-edit/${props.id}`);
  };

  const handleOnKeyPress = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.key === 'Enter') {
      handleOnClick();
    }
  };

  useEffect(() => {
    const fullName = `${owner.firstName} ${owner.lastName}`;
    setOwnerFullName(fullName);
  }, []);

  const { currentUser } = useAuth();

  useEffect(() => {
    if (currentUser?.id === owner.id) {
      dispatch({ type: 'edit' });
    }
    if (currentUser?.id === owner.id) {
      dispatch({ type: 'edit' });
    }
  }, []);

  const EventButton = () => (
    <Button
      text={state.text}
      style={state.style}
      size={ButtonSize.Small}
      onClick={state.onClick}
    />
  );

  return (
    renderEventComponent({
      ...props, ownerFullName, onClick: handleOnClick, onKeyPress: handleOnKeyPress, EventButton,
    })
  );
};

BaseEvent.displayName = 'BaseEvent';
