import { Paper } from 'components/Paper';
import styled from 'styled-components';

export const StyledPaper = styled(Paper)`
    width: 100%;
    padding: 20px 32px;
`;
