import { Color } from 'assets/css/colors';
import styled from 'styled-components';

export const StyledHeader = styled.div`
    font-size: 22px;
    color: ${Color.Black}
`;
