/* eslint-disable no-console */
import React from 'react';
import {
  text, number, object,
} from '@storybook/addon-knobs';
import { EventRow } from './EventRow';

export const eventRow = () => {
  const returnTitle = text('Event Card title', 'Storybook 101');
  const returnDescription = text('Event Card description', 'I will teach storybook to whole mankind');
  const returnStartsAt = text('Event Card starting date', '15.05.2015');
  const returnCapacity = number('Event Card capacity', 50);
  const returnAttendees = number('Event Card attendees', 5);
  const returnOwner = object('Event Card owner', {
    firstName: 'Rudolf',
    lastName: 'Robinson',
    id: '10',
    updatedAt: '2016-12-08T10:46:33.901Z',
    createdAt: '2016-12-08T10:46:33.901Z',
    email: 'emai.@email.com',
  });

  const mockedID = Math.random().toString();

  return (
    <EventRow
      title={returnTitle}
      description={returnDescription}
      startsAt={returnStartsAt}
      capacity={returnCapacity}
      attendees={returnAttendees}
      owner={returnOwner}
      key={mockedID}
      id={mockedID}
    />
  );
};

export default {
  title: 'Event',
  component: EventRow,
};
