import { Color } from 'assets/css/colors';
import React from 'react';
import { Row } from 'layouts/wrappers/Row';
import { Alignment } from 'layouts/wrappers/enums/alignment';
import PropTypes from 'prop-types';
import { StyledPaper } from './components/StyledPaper';
import { BaseEvent, IBaseEventProps } from '../BaseEvent';
import { StyledHeader } from './components/StyledHeader';

export interface IEventRowProps extends Omit<IBaseEventProps, 'renderEventComponent'>{}

const propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  startsAt: PropTypes.string.isRequired,
  capacity: PropTypes.number.isRequired,
  attendees: PropTypes.number.isRequired,
  id: PropTypes.string.isRequired,
};

export const EventRow: React.FC<IEventRowProps> = (props) => {
  const {
    title,
    description,
    startsAt,
    capacity,
    attendees,
    id,
  } = props;

  const atendees = `${capacity} of ${attendees}`;

  return (
    <BaseEvent
      {...props}
      renderEventComponent={({
        ownerFullName, onClick, onKeyPress, EventButton,
      }) => (
        <div tabIndex={0} role="button" onClick={onClick} onKeyPress={onKeyPress} style={{ cursor: 'pointer' }}>
          <StyledPaper>
            <Row spacing={32} width="100%" alignX={Alignment.SpaceBetween}>
              <StyledHeader>{title}</StyledHeader>

              <p style={{ color: Color.GrayDark }}>{description}</p>

              <p style={{ color: Color.BlackMedium }}>{ownerFullName}</p>

              <time
                dateTime={startsAt}
                style={{
                  color: Color.GrayDate,
                }}
              >
                {startsAt}
              </time>

              <Row>
                <p style={{ color: Color.GrayDark }}>{atendees}</p>
              </Row>
              {EventButton(id)}
            </Row>
          </StyledPaper>
        </div>
      )}
    />
  );
};

EventRow.displayName = 'EventRow';
EventRow.propTypes = propTypes;
