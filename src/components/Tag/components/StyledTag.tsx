import { Color } from 'assets/css/colors';
import styled from 'styled-components';

export const StyledTag = styled.div`
    font-size: 13px;
    line-height: 31px;
    padding: 0 16px;
    width: fit-content;
    text-align: center;
    border-radius: 100px;
    color: ${Color.GrayDark};
    background-color: ${Color.GrayMedium}
`;
