import React from 'react';
import PropTypes from 'prop-types';
import { StyledTag } from './components/StyledTag';

export interface ITagProps {
  readonly text: string
}

const propTypes = {
  text: PropTypes.string.isRequired,
};

export const Tag:React.FC<ITagProps> = (props) => {
  const {
    text,
  } = props;

  return (
    <StyledTag>
      {text}
    </StyledTag>
  );
};

Tag.displayName = 'Tag';
Tag.propTypes = propTypes;
