import React from 'react';
import { text } from '@storybook/addon-knobs';
import { Tag } from './Tag';

export const tag = () => {
  const returnText = text('Tag text', 'Tag me in');
  return (
    <Tag text={returnText} />
  );
};

export default {
  title: `Tag/${Tag.displayName}`,
  component: Tag,
};
