import styled, { css } from 'styled-components';

interface IStyledGridProps{
    readonly isCardView: boolean
}

const gridCardLayout = css`
    grid-template-columns: repeat(auto-fit, 400px);
`;

export const StyledGrid = styled.div<IStyledGridProps>`
    display: grid;
    grid-gap: 15px;
    ${({ isCardView }) => (isCardView && gridCardLayout)}
    justify-items: center;
    justify-content: center;
    position: relative;
`;
