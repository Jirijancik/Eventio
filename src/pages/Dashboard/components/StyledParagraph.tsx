import { Color } from 'assets/css/colors';
import styled from 'styled-components';

export const StyledParagraph = styled.p`
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 24px;
    color:${Color.GrayDark}
`;
