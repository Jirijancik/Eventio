import React, { useEffect, useState } from 'react';
import { DashboardLayout } from 'layouts/DashboardLayout/DashboardLayout';
import { dashboardService, IEvent } from 'services/dashboardService';
import { EventRow, EventCard } from 'components/Event';
import { DashboardNavbar } from 'layouts/DashboardLayout/components/DashboardNavbar';
import { BiLoader } from 'react-icons/bi';
import { Color } from 'assets/css/colors';
import { useViewport } from 'utils/useVieport';
import { StyledGrid } from './components/StyledGrid';

export const Dashboard: React.FC = () => {
  const [data, setData] = useState([] as Array<IEvent>);
  const [isCardView, setIsCardView] = useState(true);
  const [fetchedError, setFetchedError] = useState('No events found to display');
  const [isLoading, setIsLoading] = useState(true);
  const { width } = useViewport();
  const breakpoint = 1050;

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const result = await dashboardService.getAllEvents();
        setData(result);
      } catch (error) {
        setFetchedError(error);
      } finally {
        setIsLoading(false);
      }
    };
    fetchData();
  }, []);

  const EventComponent = (isCardView || width < breakpoint) ? EventCard : EventRow;

  return (
    <DashboardLayout
      topNavContent={(
        <DashboardNavbar
          isMobile={width < breakpoint}
          onToggle={setIsCardView}
        />
      )}
    >
      {
        isLoading
          ? (
            <BiLoader
              style={{ placeSelf: 'center', padding: 90 }}
              width="100%"
              size={90}
              color={Color.Green}
            />
          )
          : (
            <StyledGrid isCardView={isCardView}>
              { data ? data.map(({
                title, description, startsAt, capacity, attendees, owner, id,
              }) => (
                <EventComponent
                  title={title}
                  description={description}
                  startsAt={startsAt}
                  capacity={capacity}
                  attendees={attendees.length}
                  owner={owner}
                  key={id}
                  id={id}
                />
              )) : { fetchedError }}
            </StyledGrid>
          )
        }
    </DashboardLayout>
  );
};

Dashboard.displayName = 'Dashboard';
