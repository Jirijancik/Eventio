import { Row } from 'layouts/wrappers/Row';
import styled from 'styled-components';

export const StyledRow = styled(Row)`
    align-items: stretch;
`;
