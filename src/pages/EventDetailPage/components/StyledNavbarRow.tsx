import { Row } from 'layouts/wrappers/Row';
import styled from 'styled-components';

export const StyledNavbarRow = styled(Row)`
    justify-content: space-between;
`;
