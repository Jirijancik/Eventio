import React, { useEffect, useState } from 'react';
import { DashboardLayout } from 'layouts/DashboardLayout/DashboardLayout';
import { useParams } from 'react-router-dom';
import { eventService } from 'services/eventService';
import { IEvent } from 'services/dashboardService';
import { Row } from 'layouts/wrappers/Row';
import { Tag } from 'components/Tag';
import { BiLoader } from 'react-icons/bi';
import { Color } from 'assets/css/colors';
import { Link } from 'components/Link';
import { Alignment } from 'layouts/wrappers/enums/alignment';
import { EventCard } from 'components/Event';
import { Paper } from 'components/Paper';
import { StyledRow } from './components/StyledRow';
import { StyledNavbarRow } from './components/StyledNavbarRow';

export const EventDetailPage: React.FC = () => {
  const [data, setData] = useState({} as IEvent);
  const [isLoading, setIsLoading] = useState(true);
  const [, setErrorr] = useState(null);
  const { id } = useParams<any>();

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const result = await eventService.getSpecificEvent(id);
        setData(result);
      } catch (e) {
        setErrorr(e);
      }
      setIsLoading(false);
    };

    fetchData();
  }, []);

  return (
    <DashboardLayout topNavContent={(
      <>
        <StyledNavbarRow spacing={10} width="100%">
          <h1>
            DETAIL EVENT: #
            {' '}
            {data.id}
          </h1>
          <Link linkPath="/dashboard" text="Go Back" />
        </StyledNavbarRow>
      </>
)}
    >
      {isLoading
        ? <BiLoader style={{ placeSelf: 'center', padding: 90 }} width="100%" size={90} color={Color.Green} />
        : (
          <StyledRow width="100%" wrap spacing={17} alignX={Alignment.Center}>
            <EventCard
              title={data.title}
              description={data.description}
              startsAt={data.startsAt}
              capacity={data.capacity}
              attendees={data.attendees.length}
              owner={data.owner}
              key={data.id}
              id={data.id}
            />
            <Paper spacing={32}>
              <h1>Attendees</h1>
              <Row wrap width="300px" spacing={8}>
                {data?.attendees.map((item) => <Tag text={`${item.firstName} ${item.lastName}`} key={item.id} />)}
              </Row>
            </Paper>
          </StyledRow>
        )}
    </DashboardLayout>
  );
};

EventDetailPage.displayName = 'EventDetailPage';
