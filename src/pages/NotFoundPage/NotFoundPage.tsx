import { Button } from 'components/Button';
import { ButtonStyle } from 'components/Button/enums/buttonStyleEnum';
import { StartUpLayout } from 'layouts/StartUpLayout/StartUpLayout';
import { Alignment } from 'layouts/wrappers/enums/alignment';
import { Stack } from 'layouts/wrappers/Stack';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { StyledHeader } from './components/StyledHeader';
import { StyledParagraph } from './components/StyledParagraph';

export const NotFoundPage: React.FC = () => {
  const history = useHistory();

  return (
    <StartUpLayout>
      <Stack alignX={Alignment.Center} alignY={Alignment.Start}>
        <StyledHeader>404 Error - page not found</StyledHeader>
        <StyledParagraph>
          Seems like Darth Vader just hits our website and drops it down.
          <br />
          Please press the refresh button and everything should be fine again.
        </StyledParagraph>
        <Button
          text="GO BACK"
          onClick={() => history.goBack()}
          style={ButtonStyle.Secondary}
        />
      </Stack>
    </StartUpLayout>
  );
};

NotFoundPage.displayName = 'NotFoundPage';
