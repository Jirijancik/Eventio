import { Color } from 'assets/css/colors';
import styled from 'styled-components';

export const StyledHeader = styled.h1`
    font-style: normal;
    font-weight: normal;
    font-size: 28px;
    line-height: 48px;
    color: ${Color.Black}
`;
