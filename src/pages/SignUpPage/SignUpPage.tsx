import React, { useEffect, useRef, useState } from 'react';
import { StartUpLayout } from 'layouts/StartUpLayout/StartUpLayout';
import { Input } from 'components/Input';
import { Button } from 'components/Button';
import { InputTypesEnum } from 'components/Input/enums/InputTypesEnum';
import { ButtonStyle } from 'components/Button/enums/buttonStyleEnum';
import { ButtonSize } from 'components/Button/enums/buttonSizesEnum';
import { Stack } from 'layouts/wrappers/Stack';
import { Alignment } from 'layouts/wrappers/enums/alignment';
import { useAuth } from 'contexts/AuthContext';
import { ButtonType } from 'components/Button/enums/buttonTypeEnum';
import { useHistory } from 'react-router-dom';
import { StyledHeader } from './components/StyledHeader';
import { StyledParagraph } from './components/StyledParagraph';

export const SignUpPage: React.FC = () => {
  const [isLoading, setIsLoading] = useState(false);

  const emailRef = useRef(document.createElement('input'));
  const passwordRef = useRef(document.createElement('input'));
  const passwordRepeatedRef = useRef(document.createElement('input'));
  const firstNameRef = useRef(document.createElement('input'));
  const lastNameRef = useRef(document.createElement('input'));

  const history = useHistory();

  const { signUp, authenticated } = useAuth();

  const handleOnSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    try {
      setIsLoading(true);
      await signUp!(
        firstNameRef.current.value,
        lastNameRef.current.value,
        emailRef.current.value,
        passwordRef.current.value,
      );
    } catch (fetchError) {
      setIsLoading(false);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    if (authenticated) {
      history.push('/dashboard');
    }
  }, [authenticated]);

  return (
    <StartUpLayout>
      <Stack spacing={64} alignX={Alignment.Center} alignY={Alignment.Start}>
        <Stack spacing={6}>
          <StyledHeader>Get started absolutely free.</StyledHeader>
          <StyledParagraph>Enter your details below.</StyledParagraph>
        </Stack>

        <form onSubmit={handleOnSubmit} id="my-form">
          <Stack spacing={39}>
            <Input disabled={isLoading} label="First name" placeholder="First name" type={InputTypesEnum.Text} ref={firstNameRef} />
            <Input disabled={isLoading} label="Last name" placeholder="Last name" type={InputTypesEnum.Text} ref={lastNameRef} />
            <Input disabled={isLoading} label="Email" placeholder="Email" type={InputTypesEnum.Email} ref={emailRef} />
            <Input disabled={isLoading} label="Password" placeholder="Password" type={InputTypesEnum.Password} ref={passwordRef} />
            <Input disabled={isLoading} label="Repeat password" placeholder="Repeat password" type={InputTypesEnum.Password} ref={passwordRepeatedRef} />
          </Stack>
        </form>

        <Button
          text="SIGN UP"
          onClick={handleOnSubmit}
          type={ButtonType.Submit}
          style={ButtonStyle.Primary}
          size={ButtonSize.Large}
          isLoading={isLoading}
          form="my-form"
        />
      </Stack>
    </StartUpLayout>
  );
};

SignUpPage.displayName = 'SignUpPage';
