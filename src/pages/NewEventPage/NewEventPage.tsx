import { Button, IconButton } from 'components/Button';
import { ButtonSize } from 'components/Button/enums/buttonSizesEnum';
import { ButtonStyle } from 'components/Button/enums/buttonStyleEnum';
import { Input } from 'components/Input';
import { InputTypesEnum } from 'components/Input/enums/InputTypesEnum';
import { Paper } from 'components/Paper';
import { BasicLayout } from 'layouts/BasicLayout/BasicLayout';
import { Alignment } from 'layouts/wrappers/enums/alignment';
import { Stack } from 'layouts/wrappers/Stack';
import React, { useRef, useState } from 'react';
import { MdClose } from 'react-icons/md';
import { useHistory } from 'react-router-dom';
import { eventService } from 'services/eventService';

export const NewEventPage: React.FC = () => {
  const [isLoading, setIsLoading] = useState(false);

  const titleRef = useRef(document.createElement('input'));
  const descriptionRef = useRef(document.createElement('input'));
  const dateRef = useRef(document.createElement('input'));
  const timeRef = useRef(document.createElement('input'));
  const capacityRef = useRef(document.createElement('input'));

  const history = useHistory();

  const handleOnSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault();
    try {
      setIsLoading(true);
      await eventService.postNewEvent(
        titleRef.current.value,
        descriptionRef.current.value,
        dateRef.current.value,
        parseInt(capacityRef.current.value, 10),

      );
    } catch (fetchError) {
      setIsLoading(false);
    }
    history.push('/dashboard');
  };

  const CloseButton = (
    <IconButton
      label="Close"
      onClick={() => history.goBack()}
      renderIcon={() => <MdClose />}
      size={ButtonSize.Raw}
    />
  );

  return (
    <BasicLayout topLeftCornerContent={CloseButton}>
      <Paper spacing={40}>
        <Stack spacing={40} alignX={Alignment.Center} alignY={Alignment.Center}>

          <Stack>
            <h1>Create new event</h1>
            <p>Enter details bellow</p>
          </Stack>

          <Input disabled={isLoading} label="Title" placeholder="Title" ref={titleRef} />
          <Input disabled={isLoading} label="Description" placeholder="Description" ref={descriptionRef} />
          <Input disabled={isLoading} label="Date" placeholder="Date" type={InputTypesEnum.Date} ref={dateRef} />
          <Input disabled={isLoading} label="Time" placeholder="Time" type={InputTypesEnum.Time} ref={timeRef} />
          <Input disabled={isLoading} label="Capacity" type={InputTypesEnum.Number} placeholder="Capacity" ref={capacityRef} />

          <Button
            text="CREATE NEW EVENT"
            onClick={handleOnSubmit}
            style={ButtonStyle.Primary}
            size={ButtonSize.Large}
          />
        </Stack>
      </Paper>
    </BasicLayout>
  );
};

NewEventPage.displayName = 'NewEventPage';
