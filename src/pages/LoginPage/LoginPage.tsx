import React, { useRef, useState, useEffect } from 'react';
import { StartUpLayout } from 'layouts/StartUpLayout/StartUpLayout';
import { Input } from 'components/Input';
import { Button } from 'components/Button';
import { InputTypesEnum } from 'components/Input/enums/InputTypesEnum';
import { ButtonStyle } from 'components/Button/enums/buttonStyleEnum';
import { ButtonSize } from 'components/Button/enums/buttonSizesEnum';
import { Stack } from 'layouts/wrappers/Stack';
import { Alignment } from 'layouts/wrappers/enums/alignment';
import { useHistory } from 'react-router-dom';
import { useAuth } from 'contexts/AuthContext';
import { Color } from 'assets/css/colors';
import { StyledHeader } from './components/StyledHeader';
import { StyledParagraph } from './components/StyledParagraph';

export const LoginPage: React.FC = () => {
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const { signIn, authenticated } = useAuth();
  const emailRef = useRef(document.createElement('input'));
  const passwordRef = useRef(document.createElement('input'));
  const history = useHistory();

  const handleOnClick = async () => {
    try {
      setIsLoading(true);
      await signIn!(emailRef.current.value, passwordRef.current.value);
    } catch (fetchError) {
      setIsLoading(false);
    }
    setIsLoading(false);
    return await authenticated ? setError(false) : setError(true);
  };

  useEffect(() => {
    if (authenticated) {
      history.push('/dashboard');
    }
  }, [authenticated]);

  return (
    <StartUpLayout>
      <Stack spacing={64} alignX={Alignment.Center} alignY={Alignment.Start}>
        <Stack spacing={6}>
          <StyledHeader>Sign in to Eventio.</StyledHeader>
          <StyledParagraph>Enter your details below.</StyledParagraph>
        </Stack>

        <Input disabled={isLoading} label="Email" placeholder="Email" type={InputTypesEnum.Email} ref={emailRef} />
        <Input disabled={isLoading} label="Password" placeholder="Password" type={InputTypesEnum.Password} ref={passwordRef} />

        <Button
          text="SIGN IN"
          onClick={handleOnClick}
          style={ButtonStyle.Primary}
          size={ButtonSize.Large}
          isLoading={isLoading}
        />
        {error && (
        <p style={{ color: Color.Pink }}>
          Email or Password do not match, please try again.
        </p>
        )}
      </Stack>
    </StartUpLayout>
  );
};

LoginPage.displayName = 'LoginPage';
