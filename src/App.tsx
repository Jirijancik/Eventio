import React from 'react';
import {
  BrowserRouter as Router, Redirect, Route, Switch,
} from 'react-router-dom';
import { LoginPage } from 'pages/LoginPage/LoginPage';
import './assets/css/global.styles.css';
import { SignUpPage } from 'pages/SignUpPage/SignUpPage';
import { Dashboard } from 'pages/Dashboard/Dashboard';
import { PrivateRoute } from 'router/PrivateRoute/PrivateRoute';
import { AuthProvider, useAuth } from 'contexts/AuthContext';
import { EventDetailPage } from 'pages/EventDetailPage/EventDetailPage';
import { NotFoundPage } from 'pages/NotFoundPage/NotFoundPage';
import { NewEventPage } from 'pages/NewEventPage/NewEventPage';

function App() {
  const { authenticated } = useAuth();
  return (
    <AuthProvider>
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/">
              {!authenticated
                ? <Redirect to="/login" />
                : <PrivateRoute path="/dashboard" exact component={Dashboard} />}
            </Route>
            <Route path="/login" exact component={LoginPage} />
            <Route path="/sign-up" exact component={SignUpPage} />
            <PrivateRoute path="/dashboard" exact component={Dashboard} />
            <PrivateRoute path="/dashboard/future-events" exact component={Dashboard} />
            <PrivateRoute path="/dashboard/event-edit/:id" exact component={EventDetailPage} />
            <PrivateRoute path="/dashboard/new-event" exact component={NewEventPage} />
            <PrivateRoute path="/" component={NotFoundPage} />
          </Switch>
        </Router>
      </div>
    </AuthProvider>
  );
}

export default App;
