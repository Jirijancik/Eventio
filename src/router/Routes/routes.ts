import { Dashboard } from 'pages/Dashboard/Dashboard';
import { EventDetailPage } from 'pages/EventDetailPage/EventDetailPage';
import { NewEventPage } from 'pages/NewEventPage/NewEventPage';
import { LoginPage } from 'pages/LoginPage/LoginPage';
import { SignUpPage } from 'pages/SignUpPage/SignUpPage';

export const routesConfig = [
  {
    path: '/login',
    component: LoginPage,
    title: 'Log in to Eventio',
    needsAuth: false,
    id: 0,
  },
  {
    path: '/sign-up',
    component: SignUpPage,
    title: 'Create new account',
    needsAuth: false,
    id: 0,
  },
  {
    path: '/dashboard',
    component: Dashboard,
    title: 'Dashboard',
    needsAuth: true,
    id: 10,
  },
  {
    path: '/dashboard/future-events',
    component: Dashboard,
    title: 'Future Events',
    needsAuth: true,
    id: 11,
  },
  {
    path: '/dashboard/event-edit/:id',
    component: EventDetailPage,
    title: 'Event Detail',
    needsAuth: true,
    id: 12,
  },
  {
    path: '/dashboard/new-event',
    component: NewEventPage,
    title: 'Add new Event',
    needsAuth: true,
    id: 13,
  },
];
