import { routesConfig } from './routes';

export interface IRoute {
    path: string,
    component: React.ElementType,
    title: string,
    needsAuth: boolean,
    id: number,
}

export const routes: IRoute[] = routesConfig;
