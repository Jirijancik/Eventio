/* eslint-disable react/prop-types */
import { useAuth } from 'contexts/AuthContext';
import React from 'react';
import {
  Redirect, Route, RouteProps,
} from 'react-router-dom';

interface PrivateRouteProps extends Omit<RouteProps, 'component'> {
    component: React.ElementType;
}

export const PrivateRoute: React.FC<PrivateRouteProps> = ({
  component: Component,
  ...rest
}) => {
  const { authenticated } = useAuth();
  return (
    <Route
      {...rest}
      render={(props) => (authenticated
        ? (<Component {...props} />)
        : (<Redirect to={{ pathname: 'login', state: { from: props.location } }} />))}
    />
  );
};

PrivateRoute.displayName = 'PrivateRoute';
