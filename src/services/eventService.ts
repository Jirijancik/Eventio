import { IUser } from './authService';
import { httpService, DATA_PATH } from './httpService';

const URL = `${DATA_PATH}/events`;

export interface IEvent{
  attendees: Array<IUser>,
  capacity: number,
  createdAt: string,
  description: string,
  id: string,
  owner: IUser,
  startsAt: string,
  title: string,
  updatedAt: string
}

export const eventService = {
  async getSpecificEvent(id: string): Promise<IEvent> {
    return (await httpService.get(`${URL}/${id}`)).data;
  },

  // ISO-formatted datetime string when the event is about to start (2016-12-08T10:46:33.901Z)
  async postNewEvent(
    title: string,
    description:string,
    startsAt: string,
    capacity: number,
  ): Promise<IEvent> {
    return (await httpService.post(URL, {
      title, description, startsAt, capacity,
    })).data;
  },

  async postAttendEvent(
    id: string,
  ): Promise<IEvent> {
    return (await httpService.post(`${URL}/${id}/attendees/me`)).data;
  },

  async deleteAttendEvent(
    id: string,
  ): Promise<IEvent> {
    return (await httpService.delete(`${URL}/${id}/attendees/me`)).data;
  },
};
