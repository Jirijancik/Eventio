import { IUser } from './authService';
import { httpService, DATA_PATH, API_KEY } from './httpService';

const URL = `${DATA_PATH}/events`;

export interface IEvent{
  attendees: Array<IUser>,
  capacity: number,
  createdAt: string,
  description: string,
  id: string,
  owner: IUser,
  startsAt: string,
  title: string,
  updatedAt: string
}

export const dashboardService = {
  async getAllEvents(): Promise<Array<IEvent>> {
    return (await httpService.get(URL, {
      headers: {
        APIKey: API_KEY,
      },
    })).data;
  },
};
