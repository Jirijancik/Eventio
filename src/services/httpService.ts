/* eslint-disable consistent-return */
/* eslint-disable no-param-reassign */
/* eslint-disable @typescript-eslint/no-unused-vars */
import axios from 'axios';
import LocalStorageService from './localStorageService';

// export const DATA_PATH = 'https://private-anon-b47648b407-strvtestprojectv2.apiary-proxy.com';
export const DATA_PATH = 'https://private-anon-b6bc3b5edc-strvtestprojectv2.apiary-mock.com/';
export const API_KEY = '592d42f0cf5d1c464337d85ef245097e2e5eb82d';

const localStorageService = LocalStorageService.getService();

const defaultHeaders = {
  APIKey: API_KEY,
  Authorization: '',
};

if (localStorage.token) {
  defaultHeaders.Authorization = localStorage.token;
}

export const httpService = axios.create({
  headers: defaultHeaders,
});

// Request Interceptor
httpService.interceptors.request.use(
  (config) => {
    const token = localStorageService.getAccessToken();
    if (token) {
      config.headers.Authorization = token;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  },
);

// Response Interceptor
httpService.interceptors.response.use((response) => response, (error) => {
  const originalRequest = error.config;

  if (error.response.status === 401 && originalRequest.url === `${DATA_PATH}/auth/native`) {
    window.location.pathname = '/login';
    return Promise.reject(error);
  }

  if (error.response.status === 401 && !originalRequest.retry) {
    originalRequest.retry = true;
    const refreshToken = localStorageService.getRefreshToken();
    return httpService.post('/auth/native',
      {
        refresh_token: refreshToken,
      })
      .then((res) => {
        if (res.status === 201) {
          localStorageService.setToken(res.data);
          httpService.defaults.headers.common.Authorization = localStorageService.getAccessToken();
          return axios(originalRequest);
        }
      });
  }
  return Promise.reject(error);
});
