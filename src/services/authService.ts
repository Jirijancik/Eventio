import { httpService, DATA_PATH } from './httpService';

const URL = `${DATA_PATH}/auth`;

export interface IUser {
    id: string,
    firstName: string,
    lastName: string,
    email: string,
    createdAt: string,
    updatedAt: string
}

export const authService = {
  async getAuthenticatedUser(email: string, password: string): Promise<IUser> {
    return (await httpService.post(`${URL}/native`, { email, password })).data;
  },
};
