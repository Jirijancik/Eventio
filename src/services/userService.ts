import { httpService, DATA_PATH, API_KEY } from './httpService';

const URL = `${DATA_PATH}/users`;

export interface IUser {
    id: string,
    firstName: string,
    lastName: string,
    email: string,
    createdAt: string,
    updatedAt: string
}

export const userService = {
  async postNewUser(
    firstName:string,
    lastName:string,
    email:string,
    password:string,
  ): Promise<any> {
    return (await httpService.post(URL, {
      firstName, lastName, email, password,
    }, {
      headers: {
        APIKey: API_KEY,
      },
    })).data;
  },
};
