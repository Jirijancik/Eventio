/* eslint-disable react/prop-types */
import React, { useContext, useState, useEffect } from 'react';
import { authService, IUser } from 'services/authService';
import { userService } from 'services/userService';

export interface IAuthProviderProps{
  children?: React.ReactElement
}

// Add enum with string for translations (probably use I18N)
interface IAuthContext {
    authenticated: boolean;
    currentUser: IUser,
    signIn: (email:string, password:string) => Promise<void>,
    signOut: () => void,
    signUp: (firstName:string, lastName:string, email:string, password:string) => Promise<void>,
    error: any
}

const defaultContext = {
  authenticated: false,
  currentUser: {} as IUser,
};

const AuthContext = React.createContext<Partial<IAuthContext>>(defaultContext);

export function useAuth() {
  return useContext(AuthContext);
}

export const AuthProvider: React.FC<IAuthProviderProps> = ({ children }) => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [value, setValue] = useState<Partial<IAuthContext>>(defaultContext);
  const [user, setUser] = useState<IUser>(defaultContext.currentUser);
  const [error, setErrorr] = useState(null);

  const signIn = async (email:string, password:string) => {
    try {
      const result = await authService.getAuthenticatedUser(email, password);
      setIsAuthenticated(true);
      setUser(result);
    } catch (e) {
      setErrorr(e);
    }
  };

  const signOut = () => {
    setIsAuthenticated(false);
    setUser({} as IUser);
  };

  const signUp = async (firstName:string, lastName:string, email:string, password:string) => {
    try {
      const newUser = await userService.postNewUser(firstName, lastName, email, password);
      setIsAuthenticated(true);
      setUser(newUser);
    } catch (e) {
      setErrorr(e);
    }
  };

  useEffect(() => {
    setValue({
      ...defaultContext,
      signIn,
      signUp,
      signOut,
      authenticated: isAuthenticated,
      currentUser: user,
      error,
    });
  }, [user]);

  return (
    <AuthContext.Provider value={value}>
      {children}
    </AuthContext.Provider>
  );
};
